import { Layout} from 'antd';
import Slider from "../component/Slider";
import React from "react";


const { Content, Footer } = Layout;
const AppLayout = (props) => {
 
  return (
    <>
   <Layout style={{ minHeight: '100vh' }}>
        <Slider/>
        <Layout className="site-layout">
          <Content style={{ margin: '0 16px' }}>
            {props.children}
          </Content>
          <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
        </Layout>
      </Layout>
    </>
  );
};

export default AppLayout;
import { Layout, Menu} from 'antd';
import React, { useEffect, useState } from "react";
import { Link} from "react-router-dom";
import { 
  PieChartOutlined, 
  FileOutlined
} from '@ant-design/icons';

const { Sider } = Layout;
const AppSlider = (props) => {

 

  return (
    <>
        <Sider >
          <div className="logo" />
          <Menu theme="dark" defaultSelectedKeys={props.current} mode="inline">
            <Menu.Item key="Home" icon={<PieChartOutlined />}>
              <Link to="/">Home</Link>
            </Menu.Item>
            <Menu.Item key="Leave" icon={<FileOutlined />}>
             <Link to="/management">Management</Link>
            </Menu.Item>
          </Menu>
        </Sider>
    </>
  );
};

export default AppSlider;
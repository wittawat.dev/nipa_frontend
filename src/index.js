import React from "react";
import { createRoot } from "react-dom/client";
import {
  createBrowserRouter,
  RouterProvider,
  Route,
  Link,
} from "react-router-dom";
import Home from "./pages/Home"
import Management from "./pages/Management"
import './index.css';

const router = createBrowserRouter([
  {
    path: "/",
    element: (<Home/>),
  },
  {
    path: "management",
    element: (<Management/>),
  },
]);

createRoot(document.getElementById("root")).render(
  <RouterProvider router={router} />
);
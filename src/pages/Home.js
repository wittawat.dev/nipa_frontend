
import Layout from "../component/Layout";
import { Typography, Table, Col, Row,Form, Input, Button, Select ,Modal,Tag } from 'antd';
import React, { useEffect, useState } from "react";
import axios from "axios";
import moment from "moment";

const { Title } = Typography;
const { Option } = Select;

const Home = () => {
  const [statusFilter, setStatusFilter] = useState('');
  const [reload, setReload] = useState(1);
  const [data, setData] = useState([]);
  const [visible, setVisible] = useState(false);

  const [form] = Form.useForm();
  const statusOptions = [
    { value: "Pending", label: "Pending" },
    { value: "Accepted", label: "Accepted" },
    { value: "Resolved", label: "Resolved" },
    { value: "Rejected", label: "Rejected" },
  ];
  const columns = [
    {
      title: 'ชื่อตั๋ว',
      dataIndex: 'title',
      key: 'title',
    },
    {
      title: 'รายละเอียด',
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: 'สถานะ',
      dataIndex: 'status',
      key: 'status',
      render: (status) => {
        let color;
        let tag;
        switch (status) {
          case 'Pending':
            color = 'orange';
            tag = 'Pending';
            break;
          case 'Accepted':
            color = 'green';
            tag = 'Accepted';
            break;
          case 'Resolved':
            color = 'blue';
            tag = 'Resolved';
            break;
          case 'Rejected':
            color = 'red';
            tag = 'Rejected';
            break;
          default:
            color = 'gray';
            tag = 'Unknown';
        }
        return <Tag color={color}>{tag}</Tag>;
      },
    },
    {
      title: 'วันที่',
      dataIndex: 'create_date',
      key: 'create_date',
      render: (date) => moment(date).format("DD/MM/YYYY HH:mm:ss")
    },
  ];

  const handleCancel = () => {
    setVisible(false);
  };

  const onFinish = (values) => {
    axios.post('http://localhost:5000/ticket', values)
      .then(response => {
        console.log(response);
      })
      .catch(error => {
        console.error(error);
      });
    setReload(reload+1);
    setVisible(false);
  };
  const filteredData = data.filter((item) =>
    statusFilter ? item.status === statusFilter : true
  );
  useEffect(() => {
    axios.get('http://localhost:5000/ticket')
      .then(response => {
        setData(response.data);
      })
      .catch(error => {
        console.error(error);
      });
  }, []);
  useEffect(() => {
    axios.get('http://localhost:5000/ticket')
      .then(response => {
        setData(response.data);
      })
      .catch(error => {
        console.error(error);
      });
  }, [reload]);
  
  return (
    <>
      <Layout > 
          <Title level={2}>Ticket</Title>
        <Row justify="space-between">
          <Col  span={4} style={{ marginBottom: '20px' }}>
            <Select value={statusFilter} onChange={setStatusFilter}>
              <Option value="">ทั้งหมด</Option>
              <Option value="Pending">Pending</Option>
              <Option value="Accepted">Accepted</Option>
              <Option value="Resolved">Resolved</Option>
              <Option value="Rejected">Rejected</Option>
            </Select>
          </Col>
          <Col  span={4} style={{ textAlign: 'right',marginBottom: '20px' }}>
            <Button onClick={() => setVisible(true)}>+ Add</Button>
          </Col>
          <Col span={24}>
            <Table dataSource={filteredData} columns={columns} />
          </Col>
        </Row>
      </Layout>
      <Modal
        title="Add Ticket"
        open={visible}
        width={800}
        footer={null}
        onCancel={handleCancel}
      >
        <Form form={form} name="control-hooks" onFinish={onFinish}>
        <Form.Item
            name="title"
            label="Title"
            labelCol={{ span: 4 }}
            wrapperCol={{ span: 20 }}
            rules={[{ required: true }]}
          >
            <Input style={{ width: '100%' }} />
          </Form.Item>
          <Form.Item
            name="description"
            label="Description"
            labelCol={{ span: 4 }}
            wrapperCol={{ span: 20 }}
            rules={[{ required: true }]}
          >
           <Input.TextArea rows={4} style={{ width: '100%' }} />
          </Form.Item>
          <Form.Item
            name="contact_information"
            label="Contact"
            labelCol={{ span: 4 }}
            wrapperCol={{ span: 20 }}
            rules={[{ required: true }]}
          >
            <Input.TextArea rows={4} style={{ width: '100%' }} />
          </Form.Item>
          <Form.Item 
            name="status" 
            label="Status" 
            labelCol={{ span: 4 }}
            wrapperCol={{ span: 20 }}
            rules={[{ required: true }]}>
            <Select key={1}>
              {statusOptions.map((option) => (
                <Option key={option.value} value={option.value}>
                  {option.label}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            wrapperCol={{
              offset: 4,
              span: 20,
            }}
          >
            <Row justify="space-between">
              <Col  span={4}>
                <Button onClick={handleCancel}>
                  Cancel
                </Button>
              </Col>
              <Col span={4}  style={{ textAlign: 'right'}}>
                <Button htmlType="submit" type="primary" style={{ backgroundColor: '#1890ff', borderColor: '#1890ff' }}>
                  Submit
                </Button>
              </Col>
            </Row>
          </Form.Item>
        </Form>
      </Modal>
   </>
  )
}


export default Home
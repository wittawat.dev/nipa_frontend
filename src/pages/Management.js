
import Layout from "../component/Layout";
import { Typography, Table, Col, Row ,Tabs,Tag,Modal,Select } from 'antd';
import React, { useEffect, useState } from "react";
import moment from "moment";
import axios from "axios";
const { Title } = Typography;
const { TabPane } = Tabs;
const { Option } = Select;

const Home = () => {
  
  // const [statusFilter, setStatusFilter] = useState(null);
  const [reload, setReload] = useState(1);
  // const [form] = Form.useForm();
  // const [data, setData] = useState([]);
  const [pendingData, setPendingData] = useState([]);
  const [acceptedData, setAcceptedData] = useState([]);
  const [resolvedData, setResolvedData] = useState([]);
  const [rejectedData, setRejectedData] = useState([]);
  const [activeTab, setActiveTab] = useState("1");

  const [statusModalVisible, setStatusModalVisible] = useState(false); // เก็บค่า visible ของ modal
  const [selectedStatus, setSelectedStatus] = useState([]);

  const handleTabChange = (key) => {
    setActiveTab(key);
  };

  const handleStatusChangeTest = (value, record) => {
    setSelectedStatus({
      _id:record._id,
      status: value
    });
    setStatusModalVisible(true); // เปิด modal เมื่อมีการเลือกค่าใน select
  };

  // const getSelectStyle = (status) => {
  //   switch (status) {
  //     case 'Pending':
  //       return {
  //         backgroundColor: 'orange',
  //         color: 'white',
  //       };
  //     case 'Accepted':
  //       return {
  //         backgroundColor: 'green',
  //         color: 'white',
  //       };
  //     case 'Resolved':
  //       return {
  //         backgroundColor: 'blue',
  //         color: 'white',
  //       };
  //     case 'Rejected':
  //       return {
  //         backgroundColor: 'red',
  //         color: 'white',
  //       };
  //     case 'Unknown':
  //       return {
  //         backgroundColor: 'gray',
  //         color: 'white',
  //       };
  //     default:
  //       return {};
  //   }
  // };
  const columns = [
    {
      title: 'ชื่อตั๋ว',
      dataIndex: 'title',
      key: 'title',
    },
    {
      title: 'รายละเอียด',
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: 'สถานะ',
      dataIndex: 'status',
      key: 'status',
      render: (status) => {
        let color;
        let tag;
        switch (status) {
          case 'Pending':
            color = 'orange';
            tag = 'Pending';
            break;
          case 'Accepted':
            color = 'green';
            tag = 'Accepted';
            break;
          case 'Resolved':
            color = 'blue';
            tag = 'Resolved';
            break;
          case 'Rejected':
            color = 'red';
            tag = 'Rejected';
            break;
          default:
            color = 'gray';
            tag = 'Unknown';
        }
        return <Tag color={color}>{tag}</Tag>;
      },
    },
    {
      title: 'วันที่',
      dataIndex: 'create_date',
      key: 'create_date',
      render: (date) => moment(date).format("DD/MM/YYYY HH:mm:ss")
    },
    {
      title: 'เปลี่ยนสถานะ',
      dataIndex: 'status',
      key: 'status-change',
      render: (status, record) => (
        <>
        <Select
          defaultValue={status}
          onChange={(value) => handleStatusChangeTest(value, record)}
        >
          <Option value="Pending" style={{ color: 'orange' }}>Pending</Option>
          <Option value="Accepted" style={{ color: 'green' }}>Accepted</Option>
          <Option value="Resolved" style={{ color: 'blue' }}>Resolved</Option>
          <Option value="Rejected" style={{ color: 'red' }}>Rejected</Option>
        </Select>
        
        </>
      ),
    },
    // {
    //   title: 'Action',
    //   key: 'action',
    //   render: (_, record) => (
    //     <Button onClick={() => handleEdit(record)}>Edit</Button>
    //   ),
    // },
  ];

  // const onFinish = (values) => {
  //   axios.put(`http://localhost:5000/ticket/${values._id}`, values)
  //     .then(response => {
  //       console.log(response);
  //       setReload(reload+1);
  //       setVisible(false);
  //     })
  //     .catch(error => {
  //       console.error(error);
  //     });
  // };
  const handleStatusModalOk = () => {
    const values = selectedStatus;
    axios.put(`http://localhost:5000/ticket/${values._id}`, values)
    .then(response => {
      setReload(reload+1);
      setStatusModalVisible(false); // ปิด modal เมื่อกด Cancel
      setSelectedStatus(); 
    })
    .catch(error => {
      console.error(error);
    });

  };
  
  const handleStatusModalCancel = () => {
    setStatusModalVisible(false); // ปิด modal เมื่อกด Cancel
    setSelectedStatus(); // เซ็ตค่า selectedStatus เป็นค่าเดิม
  };


  useEffect(() => {
    axios.get('http://localhost:5000/ticket')
      .then(response => {

        const initialData = {
          Pending: [],
          Accepted: [],
          Resolved: [],
          Rejected: []
        };
      
        const groupedData = response.data.reduce((acc, cur) => {
          acc[cur.status].push(cur);
          return acc;
        }, initialData);
        
        setPendingData(groupedData.Pending);
        setAcceptedData(groupedData.Accepted);
        setResolvedData(groupedData.Resolved);
        setRejectedData(groupedData.Rejected);

      })
      .catch(error => {
        console.error(error);
      });
  }, []);

  useEffect(() => {
    axios.get('http://localhost:5000/ticket')
    .then(response => {
      const initialData = {
        Pending: [],
        Accepted: [],
        Resolved: [],
        Rejected: []
      };
    
        const groupedData = response.data.reduce((acc, cur) => {
          acc[cur.status].push(cur);
          return acc;
        }, initialData);
        
        setPendingData(groupedData.Pending);
        setAcceptedData(groupedData.Accepted);
        setResolvedData(groupedData.Resolved);
        setRejectedData(groupedData.Rejected);
    })
    .catch(error => {
      console.error(error);
    });
  }, [reload]);

  return (
    <>
      <Layout > 
          <Title level={2}>Ticket</Title>
        <Row>
          <Col span={24}>
            <Tabs activeKey={activeTab} onChange={handleTabChange}>
              <TabPane tab="Pending" key="1">
                <Table dataSource={pendingData} columns={columns} />
              </TabPane>
              <TabPane tab="Accepted" key="2">
                <Table dataSource={acceptedData} columns={columns} />
              </TabPane>
              <TabPane tab="Resolved" key="3">
                <Table dataSource={resolvedData} columns={columns} />
              </TabPane>
              <TabPane tab="Rejected" key="4">
                <Table dataSource={rejectedData} columns={columns} />
              </TabPane>
            </Tabs>
          </Col>
        </Row>
      </Layout>
      
<Modal
  title="Confirm Status Change"
  open={statusModalVisible}
  onOk={handleStatusModalOk}
  onCancel={handleStatusModalCancel}
  okButtonProps={{ style: { backgroundColor: '#1890ff', borderColor: '#1890ff' }, danger: true }}
>
  <p>Are you sure you want to change the status to {selectedStatus?.status}?</p>
</Modal>
      {/* <Modal
        title="Add Ticket"
        open={visible}
        width={800}
        footer={null}
      >
        <Form 
          form={form} 
          name="control-hooks" 
          onFinish={onFinish}
          initialValues={editData}
        >
        <Form.Item name="_id" hidden>
          <Input type="hidden" />
        </Form.Item>
        <Form.Item
            name="title"
            label="Title"
            labelCol={{ span: 4 }}
            wrapperCol={{ span: 20 }}
            rules={[{ required: true }]}
          >
            <Input style={{ width: '100%' }} />
          </Form.Item>
          <Form.Item
            name="description"
            label="Description"
            labelCol={{ span: 4 }}
            wrapperCol={{ span: 20 }}
            rules={[{ required: true }]}
          >
           <Input.TextArea rows={4} style={{ width: '100%' }} />
          </Form.Item>
          <Form.Item
            name="contact_information"
            label="Contact"
            labelCol={{ span: 4 }}
            wrapperCol={{ span: 20 }}
            rules={[{ required: true }]}
          >
            <Input.TextArea rows={4} style={{ width: '100%' }} />
          </Form.Item>
          <Form.Item 
            name="status" 
            label="Status" 
            labelCol={{ span: 4 }}
            wrapperCol={{ span: 20 }}
            rules={[{ required: true }]}>
            <Select >
              {statusOptions.map((option) => (
                <Option key={option.value} value={option.value}>
                  {option.label}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item wrapperCol={{ offset: 4 }}> 
            <Button htmlType="submit" style={{ marginRight: '8px' }}>
              Submit
            </Button>
            <Button onClick={handleCancel}>
              Cancel
            </Button>
          </Form.Item>
        </Form>
      </Modal> */}
   </>
  )
}


export default Home